#include <jni.h>
#include <stdio.h>
#include "HelloWorld.h"
#include <iostream>
JNIEXPORT void JNICALL Java_HelloWorld_print
  (JNIEnv *, jobject){
	printf("Hello Worldaaa!\n");
	return;
}

JNIEXPORT jdouble JNICALL Java_HelloWorld_multi01
  (JNIEnv *env, jobject thisObj, jdoubleArray a, jdoubleArray b){

    jsize alen = env->GetArrayLength(a);
    jsize blen = env->GetArrayLength(b);
    if(alen != blen){return NULL;}
    jdouble *abody = env->GetDoubleArrayElements(a,0);
    jdouble *bbody = env->GetDoubleArrayElements(b,0);
    jdouble result = 0;
    for(int i = 0; i < alen;i++){
        std::cout<<"abody  "<<abody[i]<<"\n";
         std::cout<<"Bbody  "<<bbody[i]<<"\n";
        result += abody[i]*bbody[i];
    }



    env->ReleaseDoubleArrayElements(a,abody,0);
    env->ReleaseDoubleArrayElements(b,bbody,0);
    return result;
}

JNIEXPORT jdouble JNICALL Java_HelloWorld_multi02
  (JNIEnv *env, jobject thisObj, jdoubleArray a){
jclass cls = env->GetObjectClass(thisObj);

    jmethodID mid = env->GetMethodID(cls,"getA","()[D");

        jmethodID mid1 = env->GetMethodID(cls,"vectora","()[D");
        jobject mv = env->CallObjectMethod(thisObj,mid1);
    if(mid == NULL){return -1;}
    jobject mvdata = env->CallObjectMethod(thisObj,mid);
    jdoubleArray * bbody = reinterpret_cast<jdoubleArray*>(&mvdata);
    jdouble * data = env->GetDoubleArrayElements(*bbody,0);
    jdouble * abody = env->GetDoubleArrayElements(a,0);
    jsize alen = env->GetArrayLength(a);
    jdouble result = 0;
    for(int i = 0 ; i < alen ; i++){
         result+=abody[i]*data[i];
    }
    env->ReleaseDoubleArrayElements(*bbody,data,0);
    env->ReleaseDoubleArrayElements(a,abody,0);
    return result;

}

JNIEXPORT void JNICALL Java_HelloWorld_multi03
  (JNIEnv *env, jobject thisObj){




    jclass cls = env->GetObjectClass(thisObj);
     jmethodID mid1 = env->GetMethodID(cls,"vectora","()[D");
    jobject mvdata  = env->CallObjectMethod(thisObj,mid1);
    jobject mvdatb  = env->CallObjectMethod(thisObj,mid1);
    jdoubleArray * dataa = reinterpret_cast<jdoubleArray*>(&mvdata);
    jdoubleArray * datab = reinterpret_cast<jdoubleArray*>(&mvdatb);
    jdouble * vectora = env->GetDoubleArrayElements(*dataa,0);
    jdouble * vectorb = env->GetDoubleArrayElements(*datab,0);

    jmethodID amid = env->GetMethodID(cls,"setA","([D)V");
    jmethodID bmid = env->GetMethodID(cls,"setB","([D)V");
    jmethodID multi04mid = env->GetMethodID(cls,"calculate","()V");
	int asize = 1;
    jdoubleArray abody = env->NewDoubleArray(asize);
    jdoubleArray bbody = env->NewDoubleArray(asize);
    jdouble *adata = new jdouble[asize];
    jdouble *bdata = new jdouble[asize];

    for(int i = 0; i<asize;i++){
        adata[i] = vectora[i];
    }
    for(int i = 0; i<asize;i++){
         bdata[i] = vectorb[i];
     }

     env->SetDoubleArrayRegion(abody,0,asize,&adata[0]);
     env->SetDoubleArrayRegion(bbody,0,asize,&bdata[0]);
	 env->CallObjectMethod(thisObj,amid,abody);
	 env->CallObjectMethod(thisObj,bmid,bbody);
	 env->CallObjectMethod(thisObj,multi04mid);
     env->ReleaseDoubleArrayElements(abody,adata,0);
     env->ReleaseDoubleArrayElements(bbody,bdata,0);


    //wywolujemy multi04
    return;
}
